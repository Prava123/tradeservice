package com.stock.demo.service.test;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;

import com.stock.demo.dto.LoginDto;
import com.stock.demo.dto.ResponseDto;
import com.stock.demo.dto.StocksInfo;
import com.stock.demo.entity.Stock;
import com.stock.demo.entity.User;
import com.stock.demo.exception.InvalidUser;
import com.stock.demo.repository.StockRepository;
import com.stock.demo.repository.UserRepository;
import com.stock.demo.serviceimpl.UserServiceImpl;

 

@RunWith(PowerMockRunner.class)

 

public class UserServiceTest {

 

    @InjectMocks
    UserServiceImpl userService;

 

    @Mock
    StockRepository stockRepo;

 

    @Mock
    UserRepository userRepo;

 

    @Test
    public void setUp() {

 

    }

 

    @Test
    public void getAllStocksTest() {

 

        List<Stock> listStocks = new ArrayList<Stock>();

 

        Stock stock = new Stock();
        stock.setStockCode("TCS001");
        stock.setStockDesc("TCS Description");
        stock.setStockId(2l);
        stock.setQuantity(5);
        stock.setTrend("positive");

 

        listStocks.add(stock);

 

        StocksInfo stock1 = new StocksInfo();
        stock1.setStockCode("Infosysy COMPY");
        stock1.setStockDesc("Infosysy Description");
        stock1.setStockId(1l);
        stock1.setStockQuantity(15);
        stock1.setTrend("Pos");

 

        Mockito.when(stockRepo.findAll()).thenReturn(listStocks);

 

        List<StocksInfo> listOfStocks = userService.getStocksInfo();

 

        assertEquals(listStocks.size(), listOfStocks.size());

 

    }

 

    @Test(expected = InvalidUser.class)
    public void authencateNegativeTest() {

 

        LoginDto dto = new LoginDto();
        dto.setUserName("Ram");
        dto.setPassword("Password");

 

        User user = new User();
        user.setPassword("Ram");
        user.setUserName("Ram");

 

        Mockito.when(userRepo.findByUserName(Mockito.anyString())).thenReturn(user);

 

        ResponseDto response = userService.login(dto);

 

        assertEquals(HttpStatus.ACCEPTED, response.getCode());

 

    }

 

    @Test
    public void authencatePositiveTest() {

 

        LoginDto dto = new LoginDto();
        dto.setUserName("Ram");
        dto.setPassword("Password");

 

        User user = new User();
        user.setPassword("Password");
        user.setUserName("Ram");

 

        Mockito.when(userRepo.findByUserName(dto.getUserName())).thenReturn(user);

 

        ResponseDto response = userService.login(dto);

 

        assertEquals(HttpStatus.ACCEPTED.value(), response.getCode().intValue());

 

    }

 

}