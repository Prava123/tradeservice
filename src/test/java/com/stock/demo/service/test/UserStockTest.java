package com.stock.demo.service.test;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import com.stock.demo.entity.UserStock;
import com.stock.demo.repository.UserStockRepository;

import com.stock.demo.serviceimpl.UserStockServiceImpl;
@RunWith(PowerMockRunner.class)
public class UserStockTest {
	
	@InjectMocks
	UserStockServiceImpl userStockService;
	
	@Mock
	UserStockRepository userStockRepository;
	
	

	@Test
	public void testGetUserStockDetails() {
		
		List<UserStock> userStockList = new ArrayList<UserStock>();
		UserStock userStock = new UserStock();
		
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
        Timestamp currentTimestamp = new Timestamp(now.getTime());
		userStock.setQuantity(10);
		userStock.setStockDate(currentTimestamp);
		userStock.setStockId(1L);
		userStock.setStockPrice(3000.0);
		userStock.setStockStatus("buy");
		userStockList.add(userStock);
		
		Mockito.when(userStockRepository.findAllDetails(Mockito.anyLong(),Mockito.any(Timestamp.class))).thenReturn(Optional.of(userStockList));
		
		userStockService.getUserStockDetails(1L);
		
		//assertEquals(10,userStockList.get(0).getQuantity());
		assertEquals(10,userStock.getQuantity().intValue());
		
	}

}
