package com.stock.demo.feignclient;


import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.stock.demo.entity.Stock;

@FeignClient(value="stock-service", url="http://localhost:8081/stocks")
//@FeignClient(name="http://ORDER-SERVICE/demo/orders")
public interface StockClient {
	
	@GetMapping("")
	public List<Stock> getAll();
	
	@GetMapping("/{stockId}")
	public Optional<Stock> getStock(@PathVariable("stockId") long stockId);
}