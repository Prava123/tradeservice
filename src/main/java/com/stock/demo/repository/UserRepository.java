package com.stock.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

 

import com.stock.demo.entity.User;

 

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

 

    User findByUserName(String userName);

 

}
 