package com.stock.demo.repository;


import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.stock.demo.entity.UserStock;

@Repository
public interface UserStockRepository extends JpaRepository<UserStock, Long> {
	
	@Query(value = "SELECT * FROM  user_stock where user_id = ?1 and stock_date > ?2 ", nativeQuery = true)
	 public Optional<List<UserStock>> findAllDetails(Long userId,Timestamp ts);
}