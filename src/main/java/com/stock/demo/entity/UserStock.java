package com.stock.demo.entity;


import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "User_Stock")
public class UserStock {
	@Id
	@Column(name = "user_stock_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userStockId;
	@Column(name = "stock_id")
	private Long stockId;
	@Column(name = "stock_price")
	private Double stockPrice;
	@Column(name = "stock_date")
	private Timestamp stockDate;
	@Column(name = "user_id")
	private Long userId;
	@Column(name = "quantity")
	private Integer quantity;
	@Column(name = "stock_status")
	private String stockStatus;
	
	public Long getUserStockId() {
		return userStockId;
	}
	public void setUserStockId(Long userStockId) {
		this.userStockId = userStockId;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Double getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}
	
	public Timestamp getStockDate() {
		return stockDate;
	}
	public void setStockDate(Timestamp stockDate) {
		this.stockDate = stockDate;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getStockStatus() {
		return stockStatus;
	}
	public void setStockStatus(String stockStatus) {
		this.stockStatus = stockStatus;
	}
	public UserStock(@NotNull(message = "ID is required") Long userStockId,
			@NotEmpty(message = "stock id is required") Long stockId,
			@NotNull(message = "stock price is required") Double stockPrice,
			@NotNull(message = "date is required") Timestamp stockDate,
			@NotNull(message = "userid column is required") Long userId,
			@NotNull(message = "quantity column is required") Integer quantity,
			@NotNull(message = "stock status column is required") String stockStatus) {
		super();
		this.userStockId = userStockId;
		this.stockId = stockId;
		this.stockPrice = stockPrice;
		this.stockDate = stockDate;
		this.userId = userId;
		this.quantity = quantity;
		this.stockStatus = stockStatus;
	}
	
	public UserStock() {
	}
	
	
}