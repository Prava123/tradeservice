package com.stock.demo.serviceimpl;


import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stock.demo.dto.UserStockRequestDto;
import com.stock.demo.entity.UserStock;
import com.stock.demo.repository.UserStockRepository;
import com.stock.demo.service.UserStockService;

@Service
public class UserStockSellBuyService {

	private final UserStockRepository userRepository;
	private final static Logger logger = LoggerFactory.getLogger(UserStockService.class);

	@Autowired
	public UserStockSellBuyService(UserStockRepository userRepository) {
		this.userRepository = userRepository;
	}
     
	
//	public ResponseEntity<String> buyorSellStock(EnrollPolicyDto enrolPolicy) {
//
//		 
//
//        EmployeePolicy empPolicies = new EmployeePolicy();
//        empPolicies.setStartDate(enrolPolicy.getStartDate());
//        empPolicies.setEndDate(enrolPolicy.getEndDate());
//        Employee emp = empRepo.findByEmpId(enrolPolicy.getEmpId());
//        empPolicies.setEmployee(emp);
//        AvailablePolicy availablePolicies = availablePolicyRepo.findByPolicyId(enrolPolicy.getPolicyId());
//        empPolicies.setAvailablePolicies(availablePolicies);
//        empPolicies.setPremiumAmount(enrolPolicy.getPremiumAmount());
//        empPolicies.setPolicyNum(enrolPolicy.getPolicyNum());
//
//         employeePoliciesRepository.save(empPolicies);
//
//        return new ResponseEntity<String>("The employee insurance enrolled sucessfuly", HttpStatus.CREATED);
//    }

	
	
	@Transactional
	public UserStock saveTrade(UserStockRequestDto userStockRequestDto) {
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
        Timestamp currentTimestamp = new Timestamp(now.getTime());
        UserStock userStock= new UserStock();
		userStock.setStockId(userStockRequestDto.getStockId());
		userStock.setQuantity(userStockRequestDto.getQuantity());
		userStock.setStockPrice(userStockRequestDto.getStockPrice());
		userStock.setStockStatus(userStockRequestDto.getStockStatus());
		userStock.setUserId(userStockRequestDto.getUserId());
		userStock.setStockDate(currentTimestamp);
		logger.info("saving the product informartion in UserStock Table.");
		return userRepository.save(userStock);
	}

	public Optional<UserStock> findTrade(long id) {
		logger.info("saving the product informartion in UserStock Table.");
		return userRepository.findById(id);
	}
	
	public void sellTrade(long id) {
		logger.info("saving the product informartion in UserStock Table.");
		userRepository.deleteById(id);;
	}

}