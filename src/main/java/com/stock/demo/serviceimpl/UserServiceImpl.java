package com.stock.demo.serviceimpl;


import java.util.ArrayList;
import java.util.List;

 

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

 

import com.stock.demo.appconstant.ApplicationConstant;
import com.stock.demo.dto.ResponseDto;
import com.stock.demo.dto.StocksInfo;
import com.stock.demo.dto.LoginDto;
import com.stock.demo.entity.Stock;
import com.stock.demo.entity.User;
import com.stock.demo.exception.InvalidUser;
import com.stock.demo.repository.StockRepository;
import com.stock.demo.repository.UserRepository;
import com.stock.demo.service.UserService;

 

@Service
public class UserServiceImpl implements UserService {

 

    @Autowired
    UserRepository userRepo;

 

    @Autowired
    StockRepository stockRepo;

 

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

 

    ModelMapper mapper = new ModelMapper();

 

    @Override
    public ResponseDto login(LoginDto loginDto) {
        logger.info("validating the user login information in the userserviceimplementation");

 

        if (loginDto.getUserName() == null || loginDto.getPassword() == null) {
            ResponseDto response = new ResponseDto();
            response.setMessage("please enter the username and password,it cannot be empty");
            response.setCode(HttpStatus.BAD_REQUEST.value());
            return response;
        }

 

        User userEntity = userRepo.findByUserName(loginDto.getUserName());

 

        if (userEntity == null) {
            ResponseDto response = new ResponseDto();
            response.setMessage("please enter valid user details");
            response.setCode(HttpStatus.BAD_REQUEST.value());

 

            return response;

 

        }

 

        if (loginDto.getUserName().equals(userEntity.getUserName())
                && loginDto.getPassword().equals(userEntity.getPassword())) {
            ResponseDto vresponse = new ResponseDto();
            vresponse.setMessage("user logined successfully");
            vresponse.setCode(HttpStatus.ACCEPTED.value());

 

            return vresponse;

 

        } else {
            throw new InvalidUser(ApplicationConstant.LOGIN_FAILURE);

 

        }

 

    }

 

    @Override
    public List<StocksInfo> getStocksInfo() {

 

        List<Stock> stockInfo = stockRepo.findAll();

 

        logger.info("getting all stocks information from the UserserviceImpl");

 

        List<StocksInfo> stocksData = new ArrayList<StocksInfo>();

 

        for (Stock stock : stockInfo) {
            logger.info("getting all stocks and iterating in userserviceImpl");
            StocksInfo Stockfilter = mapper.map(stock, StocksInfo.class);
            stocksData.add(Stockfilter);

 

        }

 

        return stocksData;
    }



	
}
 