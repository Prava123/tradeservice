package com.stock.demo.serviceimpl;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.stock.demo.dto.UserStockDto;
import com.stock.demo.entity.UserStock;
import com.stock.demo.repository.UserStockRepository;
import com.stock.demo.service.UserStockService;

@Service
public class UserStockServiceImpl implements UserStockService {

	@Autowired
	UserStockRepository userStockRepository;

	public List<UserStockDto> getUserStockDetails(Long userId) {
		List<UserStockDto> userStockDtoList = null; new ArrayList<UserStockDto>();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -24);

        java.util.Date now = calendar.getTime();

        Timestamp currentTimestamp = new Timestamp(now.getTime());
		
        Optional<List<UserStock>> userStockDetails = userStockRepository.findAllDetails(userId,currentTimestamp);
		if (userStockDetails.isPresent()) {
			userStockDtoList = new ArrayList<UserStockDto>();
			for(UserStock userStock:userStockDetails.get()) {
					UserStockDto userStockDto = new UserStockDto();
					userStockDto.setStockId(userStock.getStockId());
					userStockDto.setQuantity(userStock.getQuantity());
					userStockDto.setStockPrice(userStock.getStockPrice());
					userStockDto.setStockStatus(userStock.getStockStatus());
					userStockDto.setStockDate(userStock.getStockDate());
					userStockDtoList.add(userStockDto);
				}
					
			}
			
			
		return userStockDtoList;
	}
}
