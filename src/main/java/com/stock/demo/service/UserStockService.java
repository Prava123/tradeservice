package com.stock.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.demo.dto.UserStockDto;




public interface UserStockService {

	

	public List<UserStockDto> getUserStockDetails(Long userId) ;
	
		
}
