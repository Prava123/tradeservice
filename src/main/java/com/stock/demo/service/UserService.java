package com.stock.demo.service;


import java.util.List;



import org.springframework.stereotype.Service;

 

import com.stock.demo.dto.ResponseDto;
import com.stock.demo.dto.StocksInfo;
import com.stock.demo.dto.LoginDto;

 


public interface UserService {
    public ResponseDto login(LoginDto loginDto);

 

    public List<StocksInfo> getStocksInfo();

 

}
 