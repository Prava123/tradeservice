package com.stock.demo.exception;

public class InvalidUser extends RuntimeException {

	 

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

 

    public InvalidUser() {
        super();
    }

 

    public InvalidUser(String message) {
        super(message);
    }

 

}