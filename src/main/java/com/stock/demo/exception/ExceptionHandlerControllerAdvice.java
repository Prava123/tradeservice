package com.stock.demo.exception;

import javax.servlet.http.HttpServletRequest;



import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

 

import com.stock.demo.appconstant.ApplicationConstant;

 

 

@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {

 

    @ExceptionHandler(InvalidUser.class)
    public ExceptionResponse handleException(final InvalidUser invalidUserException, final HttpServletRequest request) {

 

        ExceptionResponse error = new ExceptionResponse();
        error.setErrorMessage(invalidUserException.getMessage());
        error.setStatus(ApplicationConstant.LOGIN_FAILURE_CODE);
        return error;
    }

 

}