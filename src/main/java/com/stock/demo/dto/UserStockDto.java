package com.stock.demo.dto;

import java.sql.Timestamp;


public class UserStockDto {
		
		
		private Long stockId;
		private Double stockPrice;
		private Timestamp stockDate;
		//private Long userId;
		private Integer quantity;
		private String stockStatus;
		public Long getStockId() {
			return stockId;
		}
		public void setStockId(Long stockId) {
			this.stockId = stockId;
		}
		public Double getStockPrice() {
			return stockPrice;
		}
		public void setStockPrice(Double stockPrice) {
			this.stockPrice = stockPrice;
		}
		public Timestamp getStockDate() {
			return stockDate;
		}
		public void setStockDate(Timestamp stockDate) {
			this.stockDate = stockDate;
		}
//		public Long getUserId() {
//			return userId;
//		}
//		public void setUserId(Long userId) {
//			this.userId = userId;
//		}
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		public String getStockStatus() {
			return stockStatus;
		}
		public void setStockStatus(String stockStatus) {
			this.stockStatus = stockStatus;
		}
		
		

}
