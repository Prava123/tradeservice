package com.stock.demo.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stock.demo.dto.UserStockDto;
import com.stock.demo.service.UserStockService;

import io.swagger.annotations.ApiOperation;

@RestController

public class UserStockController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserStockController.class);
	
	@Autowired
	UserStockService userStockService;

	@ApiOperation("Search User Stock Deatils on User Id")
	@GetMapping("/users/{userid}/stocks")
	public ResponseEntity<List<UserStockDto>> getProductPriceByProductCode(@PathVariable("userid") Long userId)
			throws Exception {
		LOGGER.info("****Inside User Stock search() method ****");
		LOGGER.info("search() method called by User Id");
		List<UserStockDto> userStockDto = userStockService.getUserStockDetails(userId);
		return new ResponseEntity<>(userStockDto, HttpStatus.OK);
	}

}