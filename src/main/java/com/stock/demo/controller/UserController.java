package com.stock.demo.controller;


import java.util.List;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

 

import com.stock.demo.dto.ResponseDto;
import com.stock.demo.dto.StocksInfo;
import com.stock.demo.dto.LoginDto;
import com.stock.demo.service.UserService;

 

import io.swagger.annotations.ApiOperation;

 

@RestController
public class UserController {
    @Autowired
    UserService userService;

 

    Logger logger = LoggerFactory.getLogger(UserController.class);

 

    /**
     * Here we taking login information from the user
     * 
     * @param userName:String
     * @param password:String
     *
     *                        Once validating the loging details we return the
     *                        response to the user
     * 
     * @return message:String
     * @return ResponseCode:int
     *
     */
    @PostMapping("/login")
    @ApiOperation("authenticating the user details")
    public ResponseDto authenticate(@RequestBody LoginDto loginDto) {

 

        logger.info("user login informatio in usercontroller");
        return userService.login(loginDto);

 

    }

 

    /**
     * 
     * This api is getting the all list stocks information from the database
     * 
     * Here the api return the list of stocks information
     * 
     * @return stockId:Long
     * @return stockName:String
     * @return stockCode:String
     * @return stockDesc:String
     * @return trend:String
     * 
     * 
     */
    @GetMapping("/stocks")
    @ApiOperation("getting the all stocks information")
    public List<StocksInfo> getAllStocks() {
        return userService.getStocksInfo();

 

    }

 

}