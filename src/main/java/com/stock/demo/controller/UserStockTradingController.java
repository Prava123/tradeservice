package com.stock.demo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stock.demo.dto.UserStockRequestDto;
import com.stock.demo.entity.Stock;
import com.stock.demo.entity.UserStock;
import com.stock.demo.feignclient.StockClient;
import com.stock.demo.serviceimpl.UserStockSellBuyService;

import io.swagger.annotations.ApiOperation;

@RestController
@ApiOperation("User Controller")
@RequestMapping("/tradings")
public class UserStockTradingController {
	private static final Logger logger = LoggerFactory.getLogger(UserStockTradingController.class);

	@Autowired
	UserStockSellBuyService stockService;

	@Autowired
	StockClient stockClient;

	@ApiOperation(value = "Buying the stocks")
	@PostMapping("/buy")
	public ResponseEntity<String> buyTrade(@Valid @RequestBody UserStockRequestDto userStockRequestDto) throws Exception {

		Optional<Stock> stock = stockClient.getStock(userStockRequestDto.getStockId());
		if (stock.isPresent()) {
			Stock temp = stock.get();
			userStockRequestDto.setStockId(temp.getStockId());
			//userStock.setQuantity(temp.getQuantity());
			//userStock.setStockDate(temp.getStockDate());
			stockService.saveTrade(userStockRequestDto);
			logger.info("traded successfully");

			return ResponseEntity.status(HttpStatus.OK).body("Stock is added" );
		} else {
			return ResponseEntity.status(HttpStatus.OK).body("Internal Error" );
		}
	}

//	@ApiOperation(value = "Selling the stocks")
//	@DeleteMapping("/sell/{stockId}")
//	public ResponseEntity<String> sellTrade(@PathVariable long stockId) throws Exception {
//
//		stockService.sellTrade(stockId);
//		logger.info("traded successfully");
//
//		return ResponseEntity.status(HttpStatus.OK).body("stock sold" + stockId);
//
//	}

}
